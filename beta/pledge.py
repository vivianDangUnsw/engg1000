#!/usr/bin/python

# Import sensor object
#from Sensor import SensorControl

# Import motion controller object
from MotionControl import MotionControl

# Import OS/time tools
import sys, os, time, random

# Import ev3 objects
from ev3dev.auto import *

# Import sleep dependency
from time import sleep

# Correct relative path for OS hardware control
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

# Initialise motion controller
Motion = MotionControl( debugMode = True )

# TODO : implement sensor control and thresholds in it
Sensor #= SensorControl(threshold = , color = , motion = Motion) 

# TODO : decide speed of movements, declare and change variable as necessary 
speed = 20


if __name__ == '__main__':
	# Until a button is pressed
	print "Cmon press the button and lemme solve this maze !\n"
	while not button.any():
        pass
        
    print "Stand back and watch how it's done\n"
    sleep(3)
    
    # stops when reach the red wall.
	while Sensor.endNotReached():
	
		# robot will follow right wall if net angle turned are 0
		# move forward until wall is detected and angleTurned = 0
		if not Sensor.wallInFront() and Motion.angleTurned = 0:
			Motion.driveFwd()

		# turn left if wall in front and robot not following the right wall
		elif Motion.angleTurned = 0 :
			Motion.stopMotors()
			Motion.turnLeft(speed)
		
		# follow right wall of the maze
		if Motion.angleTurned != 0:
			if not Sensor.wallOnRight():
				Motion.stopMotors()
				Motion.turnRight()	
			elif not Sensor.wallInFront():
				Motion.moveForward()
			else:
				Motion.stopMotors()
				Motion.turnLeft()
				
	# When end reached stop the robot
	Motion.stopMotors()			
	
				
				
				
				
				

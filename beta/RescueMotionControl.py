from ev3dev.auto import *
from time import sleep
import sys, os


## @package MotionControl
#  Documentation for this module.
#
#  More details.

## Documentation for MotionControl class.
#
#
class MotionControl:
    ## Documentation for init.
    #
    #
    def __init__(self, debugMode = False ):
    	self.angleTurned = 0
        self.debug_mode   = debugMode
        self.rightMotor   = LargeMotor(OUTPUT_D)
        self.rightMotor.position = 0
        self.leftMotor    = LargeMotor(OUTPUT_A)
        self.leftMotor.position = 0
        self.sensorMotor  = MediumMotor(OUTPUT_B)
        self.sensorMotor.position = 0
    ## Documentation for a init.
    #
    #
    def stopMotors(self):
        """
        Stop motors.

        Usages;
            MotionControl.stopMotors()

        Note: this is different to brake!
        """
        # Handle debug output
        if self.debug_mode:
            # report time & direction
            print "Stopping motors"
        # Right motor stop
        self.rightMotor.stop(stop_command='brake')
        # Left motor stop
        self.leftMotor.stop(stop_command='brake')
        # sensor motor stop
        # self.sensorMotor.stop(stop_command='brake')

    def spinLeft(self, speed = 50, timeout = False, secs = 0):
        """
        Spin robot left.
        Usages;
            MotionControl.spinLeft()
            MotionControl.spinLeft( speed = 100 )
            MotionControl.spinLeft( speed = 100, timeout=True, secs=2 )
        """
        if timeout:
            # Handle debug output
            if self.debug_mode:
                # report time & direction
                print "Spinning left for {0} Seconds".format(secs)
            # Right motor forward
            self.rightMotor.run_direct(duty_cycle_sp = speed)
            # Left motor backward
            self.leftMotor.run_direct(duty_cycle_sp  = -speed)
            # until times up
            sleep(secs)
            #then stop
            self.stopMotors()
        else:
            # Handle debug output
            if self.debug_mode:
                # report direction
                print "Spinning left"
            # Right motor forward
            self.rightMotor.run_direct(duty_cycle_sp = speed)
            # Left motor backward
            self.leftMotor.run_direct(duty_cycle_sp  = -speed)
            # Until next command
        # Give a signal if we're waiting
        return True


    def spinRight(self, speed = 50, timeout = False, secs = 0):
        """
        Spin robot Right.
        Usages;
            MotionControl.spinRight()
            MotionControl.spinRight( speed = 100 )
            MotionControl.spinRight( speed = 100, timeout=True, secs=2 )
        """
        if timeout:
            # Handle debug output
            if self.debug_mode:
                # report time & direction
                print "Spinning right for {0} Seconds".format(secs)
            # Right motor backward
            self.rightMotor.run_direct(duty_cycle_sp = -speed)
            # Left motor forward
            self.leftMotor.run_direct(duty_cycle_sp  = speed)
            # until times up
            sleep(secs)
            #then stop
            self.stopMotors()
        else:
            # Handle debug output
            if self.debug_mode:
                # report direction
                print "Spinning right"
            # Right motor backward
            self.rightMotor.run_direct(duty_cycle_sp = -speed)
            # Left motor forward
            self.leftMotor.run_direct(duty_cycle_sp  = speed)
            # Until next command
        # Give a signal if we're waiting
        return True

    def driveFwd(self, speed = 50, timeout = False, secs = 0):
        """
        Drive robot forward.
        Usages;
            MotionControl.driveFwd()
            MotionControl.driveFwd( speed = 100 )
            MotionControl.driveFwd( speed = 100, timeout=True, secs=2 )
        """
        if timeout:
            # Handle debug output
            if self.debug_mode:
                # report time & direction
                print "Driving forward for {0} Seconds".format(secs)
            # Right motor forward
            self.rightMotor.run_direct(duty_cycle_sp = speed)
            # Left motor forward
            self.leftMotor.run_direct(duty_cycle_sp  = speed)
            # until times up
            sleep(secs)
            #then stop
            self.stopMotors()
        else:
            # Handle debug output
            if self.debug_mode:
                # report direction
                print "Driving forward"
            # Right motor forward
            self.rightMotor.run_direct(duty_cycle_sp = speed)
            # Left motor forward
            self.leftMotor.run_direct(duty_cycle_sp  = speed)
            # Until next command
        # Give a signal if we're waiting
        return True

    def driveBkw(self, speed = 50, timeout = False, secs = 0):
        """
        Drive robot backward.
        Usages;
            MotionControl.driveBkw()
            MotionControl.driveBkw( speed = 100 )
            MotionControl.driveBkw( speed = 100, timeout=True, secs=2 )
        """
        if timeout:
            # Handle debug output
            if self.debug_mode:
                # report time & direction
                print "Driving backward for {0} Seconds".format(secs)
            # Right motor backward
            self.rightMotor.run_direct(duty_cycle_sp = -speed)
            # Left motor backward
            self.leftMotor.run_direct(duty_cycle_sp  = -speed)
            # until times up
            sleep(secs)
            #then stop
            self.stopMotors()
        else:
            # Handle debug output
            if self.debug_mode:
                # report direction
                print "Driving backward"
            # Right motor backward
            self.rightMotor.run_direct(duty_cycle_sp = -speed)
            # Left motor backward
            self.leftMotor.run_direct(duty_cycle_sp  = -speed)
            # Until next command
        # Give a signal if we're waiting
        return True

    def turnRelative(self, speed, rotation, direction):
        """

        """
        if direction == "left":
            self.rightMotor.run_to_rel_pos(duty_cycle_sp = speed, \
                                           position_sp = rotation/0.52)
            self.leftMotor.run_to_rel_pos(duty_cycle_sp = speed, \
                                          position_sp = -rotation/0.52)
        elif direction == "right":
            self.rightMotor.run_to_rel_pos(duty_cycle_sp = speed, \
                                           position_sp = -rotation/0.52)
            self.leftMotor.run_to_rel_pos(duty_cycle_sp = speed, \
                                        position_sp = rotation/0.52)

    def circleLeft(self, speed = 50, ratio = 0.5):
        """

        """
        # Handle debug output
        if self.debug_mode:
            # report direction
            print "Circling left"
        # Right motor faster
        self.rightMotor.run_direct(duty_cycle_sp = speed)
        # Left motor slower
        self.leftMotor.run_direct(duty_cycle_sp  = speed*ratio)
        # Until next command

    def circleRight(self, speed = 50, ratio = 0.5):
        """

        """
        # Handle debug output
        if self.debug_mode:
            # report direction
            print "Circling right"
        # Right motor faster
        self.rightMotor.run_direct(duty_cycle_sp = speed*ratio)
        # Left motor slower
        self.leftMotor.run_direct(duty_cycle_sp  = speed)
        # Until next command

    def getSensorPos(self):
        return self.sensorMotor.position

    def moveSensor(self, speed, pos):
        self.sensorMotor.run_to_abs_pos(duty_cycle_sp = speed, \
                                        position_sp = pos)

	
	def turnLeft(self,speed):
		"""
		
		"""
		self.turnRelative(speed,90,"left")
		self.angleTurned = self.angleTurned + 1
		
	
	def turnRight(self,speed):
		"""
		
		"""
		self.turnRelative(speed,90,"right")
		self.angleTurned = self.angleTurned - 1
		                      
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
